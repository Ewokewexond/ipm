#!/usr/bin/env python3

# ipm - IPFS Pin Management
# Copyright (C) 2020 Savoy

# ipm is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ipm is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ipm.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
from pathlib import Path, PosixPath
import re
import sqlite3
import subprocess as sp
import socket

try:
    import argcomplete
except ImportError:
    print(
        'python3-argcomplete not found, proceeding without shell completion.\n'
        'In order to `rm` a pin, you must be in the $IPM_HOME/mounts dir.\n'
        'To remove this warning, install the above module through pip or your '
        'distro\'s package manager.')

# Environmental variables used
try:
    _ipm_home = Path(os.environ['IPM_HOME'])
    if not (_ipm_home / 'mounts').exists():
        (_ipm_home / 'mounts').mkdir()
except KeyError:
    print(
        'Set the $IPM_HOME environmental variable. This path will be where '
        'ipm will symlink your pinned IPFS items.')

# Registering adapter and converter for Decimal values instead of floats
# Adapter: Converts the Decimal value into a string
# Converter: Takes the string in SQL and turns it back into a Decimal
sqlite3.register_adapter(PosixPath, lambda a: a.as_posix())
sqlite3.register_converter("PATH", lambda c: Path(c.decode('utf-8')) if c else
                           None)

class Sql:
    def __init__(self):
        '''Connects to the SQL database, creating the tables and schema if they
        do not already exist.

        Usable through a "with" statement for proper entrance and cleanup.

        Attributes
        ----------
        db              :   str
                        Joined path of the database file taken from conf.

        conn            :   sqlite3.connection
                        Connection to the database file as specified in the
                        program's config.

        '''
        self.db = _ipm_home / 'pins.db'

        if not self.db.is_file():
            print('No database found. Creating...')

        self.conn = sqlite3.connect(
            self.db,
            detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES
        )
        cur = self.conn.cursor()

        cur.execute('''
            CREATE TABLE IF NOT EXISTS Mounts(
            hash TEXT NOT NULL PRIMARY KEY,
            type TEXT NOT NULL,
            path PATH,
            description TEXT NOT NULL,
            node TEXT NOT NULL)
            ''')
        cur.close()

    def __enter__(self):
        return self.conn

    def __exit__(self, type, value, traceback):
        self.conn.close()

def choices():
    '''Returns the local names of the IPFS pins.

    Returns
    -------
    return  :   list
                List of names of IPFS pins.

    '''
    return [x.name for x in list((_ipm_home / 'mounts').iterdir())]

def execute(cmd):
    '''Runs a given command while yielding all output as it runs.

    Parameters
    ----------
    cmd     :   list
            Shell command compatible with subprocess.

    '''
    popen = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.STDOUT,
                     universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise sp.CalledProcessError(return_code, cmd)

def check_mounts():
    '''Makes sure IPFS has mounted to its default location.

    Raises
    ------
    FileNotFoundError
                        If mount points have yet to be set up.
    '''
    if not Path('/ipfs/').is_dir() and not Path('/ipns/').is_dir():
        raise FileNotfoundError(
            'IPFS mount points do not yet exist. '
            'Please run `ipfs mount -h` for instructions in setting them up.'
        )
    if not Path('/ipfs/').is_mount() and not Path('/ipns/').is_mount():
        sp.run(['ipfs', 'mount'])

def add(path: str, desc: str=''):
    '''Adds a file/dir or an existing IPFS hash to your pinned items.

    Will also symlink the mount point for the item to $IPM_HOME/mounts/ and
    add the listing to $IPM_HOME/pins.db.

    Parameters
    ----------
    path    :   str
                Filepath of the item to be added or IPFS hash to pin.

    desc    :   str
                Description to go along with the pin. Will be used as the
                symlink filename.

    Raises
    ------
    FileNotFoundError
                If the path being added does not exist or the IPFS mount points
                have yet to be configured.

    '''
    # Checks if IPFS has yet to be mounted. The script relies on having easy
    # local file access.
    check_mounts()

    # Gets hostname to use as `node` in DB
    node = socket.gethostname()

    # Checks to see if the item being added is an IPFS hash (so it will be
    # pinned) or a local file path with the additional descriptor of file vs
    # directory.
    if path.startswith('Qm'):
        tipo, recu, ipfs_path = ('u', None, path)
        cmd = ['ipfs', 'pin', 'add']
        regex = r'pinned (Qm[A-Za-z0-9]+) recursively(?:\n|)$'
        if not desc:
            desc = path
    else:
        path = Path(path).expanduser()
        if not path.exists():
            raise FileNotFoundError('The given path does not exist')

        ft = {True: ('d', '-r'),
              False:('f', None)}
        if not desc:
            desc = path.name

        tipo, recu = ft[path.is_dir()]
        ipfs_path = path.as_posix()
        cmd = ['ipfs', 'add']
        regex = r'added (Qm[A-Za-z0-9]+) {}(?:\n|)$'.format(path.name)

    # Runs the generated IPFS command
    cmd.extend((recu, ipfs_path))
    cmd = [x for x in cmd if x is not None]
    out = []
    for line in execute(cmd):
        print(line, end='')
        if line.startswith('added') or line.startswith('pinned'):
            out.append(line)

    # Gets the final hash for the upload (so in the case of a dir, the hash of
    # the dir being uploaded).
    cdn = re.findall(regex, ''.join(out))[0]

    # Uploads to the DB for knowledge
    print(cdn, tipo, path, desc, node)
    with Sql() as conn:
        cur = conn.cursor()
        cur.execute(
            '''
            INSERT INTO Mounts
            (hash, type, path, description, node)
            VALUES(?, ?, ?, ?, ?)
            ON CONFLICT(hash)
            DO UPDATE SET
            path=excluded.path, description=excluded.description,
            node=node||';'||excluded.node
            ''', (cdn, tipo, path, desc, node)
        )
        conn.commit()

    # Links new pin to $IPM_HOME/mounts/ for easy file access
    og = Path(f'/ipfs/{cdn}')
    sym = _ipm_home / f'mounts/{desc}'
    sym.symlink_to(og)

def ls(path: str, nnn: bool=False):
    '''Lists all IPFS pins through their mount locations for easy comprehension.

    Parameters
    ----------
    nnn     :   bool
                Will list the dir through `nnn` instead of ls/exa

    '''
    path = _ipm_home / 'mounts' / path
    if nnn:
        sp.run(['nnn', '-dex', path.as_posix()])
    else:
        try:
            out = sp.run(['exa', '-abghHl', path.as_posix()])
        except FileNotFoundError:
            out = sp.run(['ls', '-lh', path.as_posix()])

def rm(path: str):
    '''Removes an IPFS pin from your local repository.

    Will also remove the symlink under $IPM_HOME/mounts/ as well as the listing
    in the database.

    Parameters
    ----------
    path    :   str
            The name of the pin as given in the database `desc` column; also
            the name used to symlink the mount. If shell completion is a
            desired feature, install python3-argcomplete and view the setup
            instructions at https://kislyuk.github.io/argcomplete/
    '''
    # Removes the symlink from your mounts directory
    path = _ipm_home / 'mounts' / path
    cdn = path.resolve().name
    path.unlink()

    # Removes the entry from the pins database
    with Sql() as conn:
        cur = conn.cursor()
        cur.execute(
            '''
            DELETE FROM Root
            WHERE hash=?
            ''', (cdn,)
        )
        conn.commit()

    # Actually remove the pin from IPFS
    out = []
    for line in execute(['ipfs', 'pin', 'rm', cdn]):
        print(line, end='')
        if line.startswith('added'):
            out.append(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='\n'.join(
        ['.__                ',
        '|__|_____   _____  ',
        '|  \____ \ /     \ ',
        '|  |  |_> >  Y Y  \\  ipm - IPFS pin management',
         '|__|   __/|__|_|  /  Source: https://codeberg.org/savoy/ipm',
        '|__|            \/'])
    )
    subparsers = parser.add_subparsers(help='additional sub-command help')

    parser_add = subparsers.add_parser('add', help='ipfs [pin] add')
    parser_add.add_argument('path', type=str, nargs='?', help=(
        'Filepath to upload or hash to pin')
    )
    parser_add.add_argument('-d', '--desc', nargs='+', help=(
        'Description of item, will be used as symlink name')
    )
    parser_add.set_defaults(func=add)

    parser_rm = subparsers.add_parser('rm', help='ipfs pin rm')
    parser_rm.add_argument('path', type=str, help=(
        'Filename of the pinned item located in $IPM_HOME/mounts'),
        metavar='path', choices=choices()
    )
    parser_rm.set_defaults(func=rm)

    parser_ls = subparsers.add_parser('ls', help='ipfs pin ls -t recursive')
    parser_ls.add_argument('path', type=str, nargs='?',
                           default=_ipm_home/'mounts', help=(
                               'Filepath of the pinned item located in '
                               '$IPM_HOME/mounts'),
                            metavar='path', choices=choices()
                           )
    parser_ls.add_argument('-n', '--nnn', action='store_true', help=(
        'Lists the pinned items through the `nnn` terminal file manager')
    )
    parser_ls.set_defaults(func=ls)

    # Will allow the script to be run without argcomplete if not installed
    try:
        argcomplete.autocomplete(parser)
    except NameError:
        pass
    args = parser.parse_args()

    # Makes sure description is a string not a list from argparse
    try:
        args.desc = ' '.join(args.desc)
    except NameError:
        pass

    try:
        args.func(**{
            key: value for key, value in vars(args).items() if key != 'func'})
    except AttributeError:
        parser.print_help()
